# Changelog


## 1.0.0 (2020-10-25)

### Tasks

* Add .gitattributes + Changelog. [Susanne Moog]

* Add documentation. [Susanne Moog]

* Add support info. [Susanne Moog]

* Use dependency graphs. [Susanne Moog]

* Add badges. [Susanne Moog]

* Add test setup. [Susanne Moog]

* Introduce Psalm. [Susanne Moog]

* Fun with CI. [Susanne Moog]

* Start CI setup. [Susanne Moog]

* Remove superfluous code. [Susanne Moog]

### Other

* Update .gitlab-ci.yml. [Susi]

* Add LICENSE. [Susi]

* Initial commit. [Susanne Moog]


